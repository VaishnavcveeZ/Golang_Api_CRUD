package main

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	"github.com/gorilla/mux"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type User struct {
	ID        primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty"`
	Name      string             `json:"name,omitempty" bson:"name,omitempty"`
	Ocupation string             `jason:"ocupation,omitempty" bson:"ocupation,omitempty"`
}

var client *mongo.Client

func CreatUser(response http.ResponseWriter, request *http.Request) {

	response.Header().Add("content-type", "application/json")
	var user User
	json.NewDecoder(request.Body).Decode(&user)
	fmt.Println("user is name ", user)
	collection := client.Database("TheGolangCRUD").Collection("people")
	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
	result, _ := collection.InsertOne(ctx, user)
	fmt.Println("result= ", result, "user = ", user)
	json.NewEncoder(response).Encode(result)

}

func getUser(response http.ResponseWriter, request *http.Request) {
	response.Header().Add("content-type", "application/json")
	var people []User
	collection := client.Database("TheGolangCRUD").Collection("people")
	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
	cursor, err := collection.Find(ctx, bson.M{})
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{"message": "` + err.Error() + `"}`))
		return
	}
	defer cursor.Close(ctx)
	for cursor.Next(ctx) {
		var user User
		cursor.Decode(&user)
		people = append(people, user)
	}
	if err := cursor.Err(); err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{"message": "` + err.Error() + `"}`))
		return
	}

	json.NewEncoder(response).Encode(people)
}

func getOneUser(response http.ResponseWriter, request *http.Request) {

	response.Header().Add("content-type", "application/json")
	params := mux.Vars(request)
	id, _ := primitive.ObjectIDFromHex(params["id"])
	fmt.Println(" id = ", id)
	var user User
	collection := client.Database("TheGolangCRUD").Collection("people")
	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
	err := collection.FindOne(ctx, User{ID: id}).Decode(&user)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{"message": "` + err.Error() + `"}`))
		return
	}
	json.NewEncoder(response).Encode(user)
}

func updateUser(response http.ResponseWriter, request *http.Request) {
	response.Header().Add("content-type", "application/json")
	var user User
	json.NewDecoder(request.Body).Decode(&user)
	id := user.ID
	name := user.Name
	occu := user.Ocupation
	collection := client.Database("TheGolangCRUD").Collection("people")
	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
	err := collection.FindOne(ctx, User{ID: id}).Decode(&user)
	result, _ := collection.UpdateOne(ctx, bson.M{"_id": id}, bson.D{{"$set", bson.D{{"name", name}, {"Ocupation", occu}}}})
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{"message": "` + err.Error() + `"}`))
		return
	}
	json.NewEncoder(response).Encode(result)
}

func deleteUser(response http.ResponseWriter, request *http.Request) {
	response.Header().Add("content-type", "application/json")
	var user User
	params := mux.Vars(request)
	id, _ := primitive.ObjectIDFromHex(params["id"])
	json.NewDecoder(request.Body).Decode(&user)
	collection := client.Database("TheGolangCRUD").Collection("people")
	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
	result, err := collection.DeleteOne(ctx, User{ID: id})
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{"message": "` + err.Error() + `"}`))
		return
	}
	json.NewEncoder(response).Encode(result)
}

func home(response http.ResponseWriter, request *http.Request) {
	response.Write([]byte("  YOU CAN USE \n----------------\n[GET]    user \n[POST]   adduser \n[GET]    oneuser/{id} \n[DELETE] deleteuser/{id} \n[PUT]    updateuser"))
	return
}

func main() {
	fmt.Println("Server Start at port : 12345 --=>")

	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
	//client, _ = mongo.Connect(ctx, options.Client().ApplyURI("mongodb://localhost:27017"))
	client, _ = mongo.Connect(ctx, options.Client().ApplyURI("mongodb+srv://vaishnav:vcvcveez@test.5ycd1.mongodb.net/TheGolangCRUD?retryWrites=true&w=majority"))
	router := mux.NewRouter()
	router.HandleFunc("/", home).Methods("GET")
	router.HandleFunc("/adduser", CreatUser).Methods("POST")
	router.HandleFunc("/user", getUser).Methods("GET")
	router.HandleFunc("/oneuser/{id}", getOneUser).Methods("GET")
	router.HandleFunc("/deleteuser/{id}", deleteUser).Methods("DELETE")
	router.HandleFunc("/updateuser", updateUser).Methods("PUT")
	http.ListenAndServe(":12345", router)

}
